# README

# Table of contents

[TOC]

# Introduction

Public temporal AUR repository. It is created to provide an alternative to the official 
[AUR repo](https://aur.archlinux.org) that are "flagged" (or just they use a name that we want use) and that we don't want
to wait for updates, maintainers replacement or we just want our own private or public temporal
solution. 

# Use

Manually:

-   `git clone ...repo`, `cd repo && makepkg` and `pacman -U ...`

With `packer` (modified):

-   [rNoz packer](https://github.com/rNoz/packer)

# New packages

The solution is completely temporal, but it works with one that I needed. 
It is provided a `utils/central_clean.info` file where we write the packages that are possible
to fetch from clients. We need to execute `utils/compress.js` to be able for clients with
`packer` (modified) parse the contents (limitation of `jshon`) and finally, we need to provide
the package directory with the `PKGBUILD` and the source tarball inside.

# Example

`packer --bbrepo g4lbb:aur --force -S nitrux-icon-theme-kde`

# License

GNU GPLv3